# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname=linux-$_flavor
pkgver=5.7.1
pkgrel=1
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="bison findutils flex installkernel openssl-dev perl"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="1dd8c1264fbaf74c500978dc231cd8b7684bda7de7378b808c5683bb23e43f7546b4d2d0547691a8e849a18904ee88fef28c9308c090eca07dc96de7a7c589f3  linux-postmarketos-qcom-msm8916-v5.7.1-msm8916.tar.gz
bbcb31710e2c44bbb33b4bf76ff82ac57fe88bdd96e256d639585ba9e2c08982819888f5cf4a27d966a8396eebe5adc104f89d297fe8fe7aa1b755ef9bdba770  config-postmarketos-qcom-msm8916.aarch64
66785323377c543c1f7e2e92cc9a8f315144b34d67c5b1ce007dda7a08cd095a4793867196261653709444590bd303e1364f697015add3cef08e3666ab509185  config-postmarketos-qcom-msm8916.armv7"
